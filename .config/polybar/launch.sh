#!/usr/bin/env bash

# Terminate already running bar istances
killall -q polybar

polybar example >> /tmp/polybar_example.log 2>&1 &
