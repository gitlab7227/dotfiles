# Fix no colors in terminal
source ~/.bashrc

# Create tmux session at login
#if [ -z "$TMUX" ]; then
#	tmux attach -t default || tmux new -s default || tmux new
#fi

if [[ "$PATH" != *"$HOME/.cargo/bin"* ]]; then
	export PATH="${PATH}:${HOME}/.cargo/bin"
fi

if [[ ":$PATH:" != *":$HOME/.local/bin:"* ]]; then
    export PATH="${PATH}:${HOME}/.local/bin"
fi
