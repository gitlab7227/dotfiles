# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory autocd nomatch
unsetopt beep

# Enable Emacs bindings
# "bindkey -v" for Vim bindings
# "bindkey -e" for Emacs bindings
bindkey -e

zstyle :compinstall filename '$HOME/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Path
if [ -e '/usr/local/cargo/bin' ]; then
    path+=('/usr/local/cargo/bin/')
    export PATH
fi

# Add zsh-syntax-highlighting
if [ -f /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]; then
    source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fi

# Enable history reverse search with Ctrl + R
bindkey '^R' history-incremental-search-backward

# Export preferred editor
if [ -e /usr/bin/vim ]; then
    export VISUAL=/usr/bin/vim
fi

# Create tmux session at login
if [ ! "$TERM_PROGRAM" = "vscode" ]; then
    if [ -z "$TMUX" ]; then
        if  `tmux has-session -t default`; then
	        tmux attach -t default
        else
	        tmux new -s default
	    fi
    fi
fi

# check if ibus-daemon is installed before executing and settings env. vars
if command -v ibus-daemon &> /dev/null; then
    ibus-daemon -drx
    # enable japanese input support
    export GTK_IM_MODULE=xim
    export XMODIFIERS=@im=ibus
    export QT_IM_MODULE=xim
fi

# Change prompt style
if [ ! "$TERM_PROGRAM" = "vscode" ]; then
    PROMPT="%F{2}%B>%b%f "
else
    PROMPT="%F{4}%B%~%b%f > "
fi

# Define aliases
if [ -x /usr/bin/dircolors ]; then
        test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
        alias ls='ls --color=auto'
        alias grep='grep --color=auto'
        alias fgrep='fgrep --color=auto'
        alias egrep='egrep --color=auto'
        alias ip='ip --color=auto'

	if [ ! "$TERM_PROGRAM" = "vscode" ]; then
	    if [ -e $(which tmux) ]; then
                alias tmux='TERM=xterm-256color tmux'
        fi
	fi
fi

# Set correct $TERM value
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# fix issue regarding Ctrl + Left/Right arrow to move between words
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

# enable bash-like comments
setopt interactivecomments
