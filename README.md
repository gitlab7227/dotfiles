# Fonts
Polybar use a bitmap font to show some icons, like the 'powermenu' one.
On some systems bitmap fonts are disabled by default so the following things:

- remove the `/etc/fonts/conf.d/70-no-bitmaps.conf` symbolic link
- add a symlink named `70-yes-bitmaps.conf` pointing to `../conf.avail/70-yes-bitmaps.conf`[[1]](#1)

# Touchpad
To enable touchpad "tapping", you can create file named `/etc/X11/xorg.conf.d/30-touchpad.conf`, containing the following lines:

```bash
Section "InputClass"
    Identifier "touchpad"
    Driver "libinput"
    MatchIsTouchpad "on"
    Option "Tapping" "on"
    Option "NaturalScrolling" "true"
EndSection
```

To make sure it is working, you can try rebooting.

# VPN
To speed up your workflow for connecting to a VPN server using openvpn, you can do do following things:

- place your configuration file (`CONNECTION_NAME.conf` or `CONNECTION_NAME.ovpn`) inside `/etc/openvpn/client/`
- rename the file to `CONNECTION_NAME.conf` (i tried using `.ovpn` but it wasn't working)
- start the VPN connection using `systemctl start openvpn-client@CONNECTION_NAME`
- then, when needed, stop it using `systemctl stop openvpn-client@CONNECTION_NAME`

# bspwm and multiple screens
If you're using a laptop with a HDMI-connected monitor, and you usually disable the laptop one, you should follow these steps to avoid bugs when using `bspwm`.

Modify `$HOME/.profile` and add the following lines:

```bash
if [ -f "$HOME/.scripts/disable_primary_screen" ]; then
    $HOME/.scripts/disable_primary_screen
fi
```

Also, make sure inside the file `$HOME/.config/bspwm/bspwmrc` you use the flag `--reset-desktops` to add desktops to your monitor, like this:

```bash
bspc monitor --reset-desktops I II III IV V VI VII VIII IX X
```

# Fix Bluetooth issues
Installing the package `pulseaudio-module-bluetooth` helped a lot when connecting a Bluetooth headset.

```bash
sudo apt-get install pulseaudio-module-bluetooth
```

## Sony WH-1000XM3 on Linux

Problem: low volume

Found a solution [here](https://unix.stackexchange.com/questions/437468/bluetooth-headset-volume-too-low-only-in-arch):

```bash
dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_38_18_4C_BF_17_62 org.bluez.MediaControl1.VolumeUp
```

To find the address `/org/bluez/hci0/dev_38_18_4C_BF_17_62` I used `d-feet`, which can be installed via `apt`.


# References
<span>[1]: <a id="1" href="https://bugs.launchpad.net/ubuntu/+source/fontconfig/+bug/1560114">Ubuntu in Launchpad</a></span>
