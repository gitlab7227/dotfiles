#r "C:\Program Files\workspacer\workspacer.Shared.dll"
#r "C:\Program Files\workspacer\plugins\workspacer.Bar\workspacer.Bar.dll"
#r "C:\Program Files\workspacer\plugins\workspacer.ActionMenu\workspacer.ActionMenu.dll"
#r "C:\Program Files\workspacer\plugins\workspacer.FocusIndicator\workspacer.FocusIndicator.dll"

using System;
using System.Diagnostics;
using workspacer;
using workspacer.Bar;
using workspacer.ActionMenu;
using workspacer.FocusIndicator;
using workspacer.Bar.Widgets;
using System.Timers;
using System.Windows;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

public class UpdateWidget : BarWidgetBase
{
    private Timer _timer;
    private int _interval;
    private string _format;

    public UpdateWidget(int interval, string format)
    {
        _interval = interval;
        _format = format;
    }

    public UpdateWidget() : this(200, "hh:mm:ss tt") { }

    public override IBarWidgetPart[] GetParts()
    {
        return Parts(DateTime.Now.ToString());
        // RAM occupied indicator
        //return Parts("");
    }

    public override void Initialize()
    {
        _timer = new Timer(_interval);
        _timer.Elapsed += (s, e) => Context.MarkDirty();
        _timer.Enabled = true;
    }
}

// https://gist.github.com/tommyready/df468fb96667fea3862e387656198c58
static void EnableAdapter(string interfaceName)
{
	ProcessStartInfo psi = new ProcessStartInfo("netsh", "interface set interface \"" + interfaceName + "\" enable");
	Process p = new Process();
	p.StartInfo = psi;
	p.Start();
}

static void DisableAdapter(string interfaceName)
{
	ProcessStartInfo psi = new ProcessStartInfo("netsh", "interface set interface \"" + interfaceName + "\" disable");
	Process p = new Process();
	p.StartInfo = psi;
	p.Start();
}

static void ResetAdapter(string interfaceName)
{
	ExecuteCommand("C:\\Users\\garnet\\Downloads\\nircmd-x64\\reset_nic.bat");	
}


static void ExecuteCommand(string command)
{
    var processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
    processInfo.CreateNoWindow = true;
    processInfo.UseShellExecute = false;
    processInfo.RedirectStandardError = true;
    processInfo.RedirectStandardOutput = true;

    var process = Process.Start(processInfo);

    process.OutputDataReceived += (object sender, DataReceivedEventArgs e) =>
        Console.WriteLine("output>>" + e.Data);
    process.BeginOutputReadLine();

    process.ErrorDataReceived += (object sender, DataReceivedEventArgs e) =>
        Console.WriteLine("error>>" + e.Data);
    process.BeginErrorReadLine();

    process.WaitForExit();

    Console.WriteLine("ExitCode: {0}", process.ExitCode);
    process.Close();
}


static void ToggleTaskbar(bool hide)
{
    if (hide)
    {
        ExecuteCommand("%USERPROFILE%\\Downloads\\nircmd-x64\\nircmd.exe win trans class Shell_TrayWnd 256");
    }
    else
    {
        ExecuteCommand("%USERPROFILE%\\Downloads\\nircmd-x64\\nircmd.exe win trans class Shell_TrayWnd 255");
    }
}

Action<IConfigContext> doConfig = (context) =>

{
    context.AddFocusIndicator(new FocusIndicatorPluginConfig()
    {
        BorderSize = 0,
        BorderColor = Color.Black
    });

    var actionMenu = context.AddActionMenu(new ActionMenuPluginConfig()
    {
        RegisterKeybind = true,
        KeybindMod = KeyModifiers.LAlt,
        KeybindKey = Keys.P,

        MenuTitle = "workspacer.ActionMenu",
        MenuHeight = 50,
        MenuWidth = 500,
        FontSize = 16,
        Matcher = new OrMatcher(new PrefixMatcher(), new ContiguousMatcher()),
        Background = Color.Black,
        Foreground = Color.White,
    });

    var subMenu = actionMenu.Create();
    subMenu.Add("do a thing", () => Console.WriteLine(""));
    subMenu.AddMenu("sub-sub menu", () => actionMenu.Create());
    subMenu.AddFreeForm("Console WriteLine", (s) => Console.WriteLine(s));


    actionMenu.DefaultMenu.AddMenu("make sub menu", subMenu);

    var newMenu = actionMenu.Create();
    // newMenu.Add("firefox", () => System.Diagnostics.Process.Start("firefox.exe"));

    newMenu.Add("Hide taskbar", () => ToggleTaskbar(true));
    newMenu.Add("Show taskbar", () => ToggleTaskbar(false));

	string interfaceName = "Ethernet"; 
	newMenu.Add("Reset adapter", () => ResetAdapter(interfaceName));

    newMenu.Add("Restart workspacer", () => context.Restart());
    newMenu.Add("Quit workpacer", () => context.Quit());

    KeyModifiers mod = KeyModifiers.Alt;

    context.Keybinds.Subscribe(mod, Keys.X, () => actionMenu.ShowMenu(subMenu));
    context.Keybinds.Subscribe(mod, Keys.Y, () => actionMenu.ShowMenu(newMenu));

    context.AddBar(new BarPluginConfig()
    {
        BarTitle = "workspacer.Bar",
        BarHeight = 25,
        FontSize = 14,
        DefaultWidgetForeground = Color.White,
        DefaultWidgetBackground = Color.Black,
        Background = Color.Black,

        // LeftWidgets = () => new IBarWidget[] { new WorkspaceWidget(), new TextWidget(": "), new TitleWidget() },
        LeftWidgets = () => new IBarWidget[] { new WorkspaceWidget()},

        // RightWidgets = () => new IBarWidget[] { new TimeWidget(), new ActiveLayoutWidget() },
        RightWidgets = () => new IBarWidget[] { new UpdateWidget()},
    });

    context.WorkspaceContainer.CreateWorkspaces("1", "2", "3", "4", "5");
    ToggleTaskbar(true);
};

return doConfig;
